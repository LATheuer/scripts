#!/usr/bin/env bash
set -euo pipefail

# Collect the files
Files=$(/usr/bin/ls)

# Ask, what the User wants to have as file Ending.
echo "Please Enter the Ending you want (Without the dot)"
read -r ENDING

# Rename the files
for File in $Files
do
    mv "$File" "$File.$ENDING"
done
