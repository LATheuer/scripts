#!/bin/bash

clear

BraucheIch=Ja

while [ $BraucheIch = Ja ]; do
    # Primary Vodafone DNS
    
    echo "Primary DNS:"
    ping -c3 195.50.140.114 
    sleep 5
    clear

    # Secondary Vodafone DNS
    echo "Secondary DNS:"
    ping -c3 195.50.140.252 
    
 

    # Wait for it to refresh
    sleep 30
    clear
done
