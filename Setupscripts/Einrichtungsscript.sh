#!/usr/bin/env bash

### Gets all neccessary stuff for the setup ###
git clone https://gitlab.com/laskar-alexander-theuer/dotfiles.git

### Installing everything ###
# Installs Programs from official Repos
sudo pacman -S --needed $(comm -12 <(pacman -Slq | sort) <(sort Packagelist)) --noconfirm
rm Packagelist

### Makes various configs available ###
cd dotfiles
cp * -Rf ../
cd ../
chmod +x .config/qtile/autostart.sh
ln -s .config/qtile/ ./qtile
cd scripts/Setupscripts
cp -f environment /etc/environment
cd ../../

### Personal Shellscripts ###
cd scripts
mv ./Schreiben.sh ../Schreibtisch/Schreiben.sh
cd ../
chmod +x Schreibtisch/Schreiben.sh


# Jahresarbeit
mkdir Jahresarbeit
cd new-adventures/
cp -R * ../Jahresarbeit
cd ../
rm -Rf Jahresarbeit/.git
rm -Rf new-adventures/

### End of Script ###
echo "Das erste Setup ist abgeschlossen. Soll jetzt neugestartet werden?"
options='Ja Nein'
select option in $options; do
    if [ "Ja" = $option ]; then
        echo "Es wird in 5 Sekunden neugestartet..."
        sleep 5
        reboot
    else
        echo "Es wird nicht neugestartet."
        exit
    fi
done
